package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter a double number for meal price");
        double meal_cost = Double.parseDouble(bufferedReader.readLine().trim());
        System.out.println("Enter a int number for tip percentage");
        int tip_percent = Integer.parseInt(bufferedReader.readLine().trim());
        System.out.println("Enter a int number for tax percentage");
        int tax_percent = Integer.parseInt(bufferedReader.readLine().trim());

        solve(meal_cost, tip_percent, tax_percent);

        bufferedReader.close();
    }
    public static void solve(double meal_cost, int tip_percent, int tax_percent) {
        double tip = meal_cost *tip_percent / 100;
        double tax = tax_percent * meal_cost / 100;
        int total_cost = (int)Math.round(meal_cost + tip + tax);
        System.out.println("Total cost: $" + total_cost);
    }
}

