package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int i = 4;
        double d = 4.0;
        String s = "HackerRank ";

        Scanner scan = new Scanner(System.in);

        int intNumber;
        double doubleNumber;
        String text;
        intNumber = Integer.parseInt(scan.nextLine());
        doubleNumber = Double.parseDouble(scan.nextLine());
        text = scan.nextLine();
        System.out.println(i + intNumber);
        System.out.println(d + doubleNumber);
        System.out.println(s + text);
        scan.close();
    }
}
